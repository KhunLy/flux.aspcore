﻿using FluxCompletAvecSecurite.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Dto
{
    public class UserDto : IPayload
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string Name { get; set; }
    }
}
