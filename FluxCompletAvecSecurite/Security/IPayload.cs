﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Security
{
    public interface IPayload
    {
        int Id { get; }
        string Email { get; }
        string RoleName { get; }
        string Name { get; }
    }
}
