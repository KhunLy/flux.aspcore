﻿using FluxCompletAvecSecurite.Entities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Repositories
{
    public class LivreRepository
    {
        private SqlConnection connection;

        public LivreRepository(SqlConnection connection)
        {
            this.connection = connection;
        }

        public void Create(Livre livre)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "INSERT INTO Livre(Nom, Auteur) VALUES (@p1, @p2)";
            cmd.Parameters.AddWithValue("p1", livre.Nom);
            cmd.Parameters.AddWithValue("p2", livre.Auteur);
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public IEnumerable<Livre> GetAll()
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM Livre";
            SqlDataReader r =  cmd.ExecuteReader();
            while(r.Read())
            {
                yield return new Livre
                {
                    Id = (int)r["id"],
                    Nom = (string)r["Nom"],
                    Auteur = (string)r["Auteur"],
                };
            }
            connection.Close();
        }

        public bool Delete(int id)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "DELETE FROM Livre WHERE Id = @p1";
            cmd.Parameters.AddWithValue("p1", id);
            int nb = cmd.ExecuteNonQuery();
            connection.Close();
            return nb != 0;
        }
    }
}
