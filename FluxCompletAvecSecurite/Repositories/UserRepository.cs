﻿using FluxCompletAvecSecurite.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Repositories
{
    public class UserRepository
    {
        private SqlConnection connection;

        public UserRepository(SqlConnection connection)
        {
            this.connection = connection;
        }

        public void Register(User u)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Register";
            cmd.Parameters.AddWithValue("Name", u.Name);
            cmd.Parameters.AddWithValue("Email", u.Email);
            cmd.Parameters.AddWithValue("PlainPassword", u.PlainPassword);
            cmd.Parameters.AddWithValue("Role", "MEMBER");
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public User Login(string email, string password)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Login";
            cmd.Parameters.AddWithValue("Email", email);
            cmd.Parameters.AddWithValue("PlainPassword", password);
            SqlDataReader r =  cmd.ExecuteReader();
            if(r.Read())
            {
                return new User
                {
                    Email = (string)r["Email"],
                    Name = (string)r["Name"],
                    Id = (int)r["Id"],
                    Role = (string)r["Role"]
                };
            }
            return null;
        }
    }
}
