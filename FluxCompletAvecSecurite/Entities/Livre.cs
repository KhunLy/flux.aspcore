﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Entities
{
    public class Livre
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Auteur { get; set; }
    }
}
