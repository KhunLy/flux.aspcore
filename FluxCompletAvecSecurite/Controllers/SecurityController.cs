﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using FluxCompletAvecSecurite.Dto;
using FluxCompletAvecSecurite.Entities;
using FluxCompletAvecSecurite.Mappers;
using FluxCompletAvecSecurite.Repositories;
using FluxCompletAvecSecurite.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace FluxCompletAvecSecurite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private JwtService jwtService;
        private UserRepository repo;
        private ILogger<SecurityController> logger;

        public SecurityController(JwtService jwtService, UserRepository repo, ILogger<SecurityController> logger)
        {
            this.jwtService = jwtService;
            this.repo = repo;
            this.logger = logger;
        }

        [HttpPost("login")]
        [Produces("application/json", Type = typeof(string))]
        public IActionResult Login(LoginDto dto)
        {
            try
            {
                User u = repo.Login(dto.Email, dto.PlainPassword);
                if(!(u is null))
                {
                    string token = jwtService.EncodeToken(u.ToDto()); 
                    return Ok(token);
                }
                return Unauthorized();
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                return Problem("Erreur Inconnue");
            }
        }

        [HttpPost("register")]
        public IActionResult Register(RegisterDto dto)
        {
            try
            {
                repo.Register(dto.ToEntity());
                return Ok();
            }
            catch(Exception e)
            {
                logger.LogError(e.Message);
                return Problem("Erreur Inconnue");
            }
        }
    }
}
