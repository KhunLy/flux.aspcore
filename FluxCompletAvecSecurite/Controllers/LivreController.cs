﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Claims;
using System.Threading.Tasks;
using FluxCompletAvecSecurite.Dto;
using FluxCompletAvecSecurite.Entities;
using FluxCompletAvecSecurite.Filters;
using FluxCompletAvecSecurite.Mappers;
using FluxCompletAvecSecurite.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FluxCompletAvecSecurite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LivreController : ControllerBase
    {
        private LivreRepository repo;
        private ILogger logger;

        public LivreController (
            LivreRepository repo,
            ILogger<LivreController> logger
        )
        {
            this.repo = repo;
            this.logger = logger;
        }

        [HttpPost]
        public IActionResult Post(LivreDto livre)
        {
            try
            {
                repo.Create(livre.ToEntity());
                return Ok();
            }
            catch(Exception e)
            {
                //loguer l'exception,
                logger.LogError(e.Message);
                return Problem("Une erreur est survenue");
            }
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                IEnumerable<Livre> l = repo.GetAll();
                return Ok(l.Select(x => x.ToDto()));
            }
            catch (Exception e)
            {
                //loguer l'exception,
                logger.LogError(e.Message);
                return Problem("Une erreur est survenue");
            }
        }

        //[HttpGet]
        //[ApiAuthorize("MEMBER", "ADMIN")]
        //public IActionResult GetByUser()
        //{
        //    try
        //    {
        //        int userId = int.Parse(User.FindFirst(ClaimTypes.PrimarySid).Value);
        //        IEnumerable<Livre> l = repo.GetAllByUser(userId);
        //        return Ok(l.Select(x => x.ToDto()));
        //    }
        //    catch (Exception e)
        //    {
        //        //loguer l'exception,
        //        logger.LogError(e.Message);
        //        return Problem("Une erreur est survenue");
        //    }
        //}

        [HttpDelete("{id}")]
        [ApiAuthorize("ADMIN")]
        public IActionResult Delete(int id)
        {
            
            try
            {
                if(repo.Delete(id)) 
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                logger.LogError(e.Message);
                return Problem("Une erreur est survenue");
            }
        }
    }
}
