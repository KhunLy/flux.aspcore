using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using FluxCompletAvecSecurite.Repositories;
using FluxCompletAvecSecurite.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FluxCompletAvecSecurite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddScoped(builder => new SqlConnection(Configuration.GetConnectionString("default")));

            services.AddScoped<LivreRepository>();
            services.AddScoped<UserRepository>();

            services.AddScoped<JwtSecurityTokenHandler>();

            services.AddScoped<JwtService>();

            services.AddCors(options => options.AddPolicy("defaultCors", builder => {
                builder.AllowAnyOrigin();
                builder.AllowAnyMethod();
                builder.AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            string path = Directory.GetCurrentDirectory();

            loggerFactory.AddFile($"{path}\\Logs\\Log.txt", LogLevel.Error);

            app.UseCors("defaultCors");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
