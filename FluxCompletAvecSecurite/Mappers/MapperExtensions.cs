﻿using FluxCompletAvecSecurite.Dto;
using FluxCompletAvecSecurite.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Mappers
{
    public static class MapperExtensions
    {
        public static Livre ToEntity(this LivreDto dto)
        {
            return new Livre
            {
                Id = dto.Id,
                Nom = dto.Nom,
                Auteur = dto.Auteur
            };
        }

        public static LivreDto ToDto(this Livre entity)
        {
            return new LivreDto
            {
                Id = entity.Id,
                Nom = entity.Nom,
                Auteur = entity.Auteur
            };
        }

        public static User ToEntity(this RegisterDto dto)
        {
            return new User
            {
                Id = dto.Id,
                Name = dto.Name,
                Email = dto.Email,
                PlainPassword = dto.PlainPassword
            };
        }

        public static UserDto ToDto(this User dto)
        {
            return new UserDto
            {
                Id = dto.Id,
                Name = dto.Name,
                Email = dto.Email,
                RoleName = dto.Role
            };
        }
    }
}
